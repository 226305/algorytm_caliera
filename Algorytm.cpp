#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <conio.h>
#include <vector>


using namespace std;

struct Dane{
	int r; //termin dostepnosci
	int p; //czas obslugi
	int q; //czas dostarczenia zadania
	int	id;
};

vector<Dane> best_pi;
int n = 0;
int Cmax=-1;
Dane e,elem,abc;
int UB=999999999; // ograniczenie gorne
int U;    // aktualny schrage
int LB;   // ograniczenie dolne

int mini(vector<Dane> &a, int id) {
	int d = a[0].r, index=0;
	for (int i=1; i < id; ++i) {
		if (a[i].r < d) {
			d = a[i].r;
			index=i;
		}
	}
	return index;
};
int maxi(vector<Dane> &a, int id) {
	int d = a[0].q, index=0;
	for (int i = 1; i < id; ++i) {
		if (a[i].q > d) {
			d = a[i].q;
			index=i;
		}
	}
	return index;
};

int shrage(int n, vector<Dane> &pi)
{
    vector<Dane> N = pi;
    pi.clear();
	int t=0, k=0, Cmax=0;
    Dane e;

	vector<Dane> G;
    while(!N.empty() || !G.empty())
    {

        while(!N.empty() && N[mini(N,N.size())].r <= t) // dopoki N!= empty i sa zadania z ri <t -> przekladamy je z N do G
        {

            e = N[mini(N,N.size())];
            G.push_back(e);
            N.erase(N.begin()+mini(N,N.size()));
            n-=1;

        }
        if(G.empty())               // jezeli G jest empty przesuwamy sie w czasie do pierwszego dostepnego zadania
        {
            t=N[mini(N,N.size())].r;
        }
        else
        {
            e=G[maxi(G,G.size())];
            k+=1;
            pi.push_back(e);      // aktualna wartosc t to czas rozpoczecia zadania e
            G.erase(G.begin()+maxi(G,G.size()));
            t+=e.p;               // t+ czas trwania zadania e to czas zakonczenia zadania e
            Cmax=max(Cmax,t+e.q);
        }

    }
    return Cmax;
}
Dane shrage_abc(int n,  vector<Dane> &pi)
{
    vector<Dane> N = pi;
    pi.clear();
    Dane wyjscie;
    int D=0,a=0;
	int iter;
	int t=0, k=0, Cmax=0;
    Dane e;
    //int poz_a, poz_b, poz_c=-1;
    vector<int> czasy;

    vector<Dane> G;

    while(!N.empty() || !G.empty())
    {

        while(!N.empty() && N[mini(N,N.size())].r <= t) // dopoki N!= empty i sa zadania z ri <t -> przekladamy je z N do G
        {

            e = N[mini(N,N.size())];
            G.push_back(e);
            N.erase(N.begin()+mini(N,N.size()));
            n-=1;

        }
        if(G.empty())               // jezeli G jest empty przesuwamy sie w czasie do pierwszego dostepnego zadania
        {
            t=N[mini(N,N.size())].r;
        }
        else
        {
            e=G[maxi(G,G.size())];
            k+=1;
            pi.push_back(e);      // aktualna wartosc t to czas rozpoczecia zadania e
            czasy.push_back(t);
            G.erase(G.begin()+maxi(G,G.size()));
            t+=e.p;               // t+ czas trwania zadania e to czas zakonczenia zadania e
            Cmax=max(Cmax,t+e.q);
            if(Cmax == t+e.q)     // Cmax sie wiekszyl?
            {
                D=k;              // nowa pozycja zadania b
            }
        }
    }
    // wyjscie.r -> a
    // wyjscie.p -> b
    // wyjscie.q -> c

    D=D-1;                        // tablice numerujemy od 0, nie od 1


    wyjscie.p =D;    // b ustalone

    wyjscie.r = wyjscie.p; // inicjuje a = b
    wyjscie.q = -1;        // inicjuje c = -1, jezeli algorytm tego nie zmieni to c nie istnieje
    int i;
    for(i=D;i>0;i--)
    {
        if(czasy[i] == czasy[i-1] + pi[i-1].p) // jezeli rozpoczecie zadania przed b + czas jego trwania = rozpoczecie b
            {
                wyjscie.r =i-1;      // to zaktualizuj a
            }
        else       // jezeli przerwana zostala ciaglosc zadan
            break; // przerwij, bo wczesniej nie bedzie a z zalozenia
    }
    for(;i<D;++i)      // zacznij od a, przesuwaj sie az do b
    {
        if(pi[i].q < pi[D].q)  // jezeli zadanie ma q mniejsze niz b
        {
            wyjscie.q = i; // znaleziono nowe c

        }
    }


    return wyjscie;   // c==-1 => c nie istnieje
}

int pre_shrage(int n, vector<Dane> pi)
{
    vector<Dane> N = pi;
    pi.clear();
    int t=0, Cmax=0;
    Dane e,l;
    l.r=0;
    l.p=0;
    l.q=999999999;

	vector<Dane> G;
     while(!N.empty() || !G.empty())
    {
        while(!N.empty() && N[mini(N,N.size())].r <= t) // dopoki N!= empty i sa zadania z ri <t -> przekladamy je z N do G
        {

            e = N[mini(N,N.size())];                    // nastepne zadanie  (o najmniejszym r)
            G.push_back(e);
            N.erase(N.begin()+mini(N,N.size()));
            //n-=1;
            if(e.q>l.q)                                 // jezeli nowe zadanie ma wiekszy czas dostarczenia to przerywamy
            {
                l.p=t-e.r;                              // policz czas jaki zostal do wykonania w starym zadaniu
                t=e.r;                                  // ustaw czas przerwania (czas dostępności e)
                if(l.p>0)                               // jezeli stare nie zostalo wykonane (zakonczone)
                {
                    G.push_back(l);                     // dodaj je do zbioru zadan gotowych do realizacji G
                }
            }
        }
        if(G.empty())               // jezeli G jest empty przesuwamy sie w czasie do pierwszego dostepnego zadania
        {
            t=N[mini(N,N.size())].r;
        }
        else
        {
            e=G[maxi(G,G.size())];
            l=e;
            pi.push_back(e);      // aktualna wartosc t to czas rozpoczecia zadania e
            G.erase(G.begin()+maxi(G,G.size()));
            t+=e.p;               // t czas trwania zadania e to czas zakonczenia zadania e
            Cmax=max(Cmax,t+e.q);
        }

    }
    return Cmax;
}

int Calier(int n, vector<Dane> pi)
{
    int identyfikator;
    int stara_wartosc;

    U = shrage(n,pi);     // gorne ograniczenie
    if ( U<UB)              // lepsze niz dotyczczas?
    {
        UB = U;             // zapamietaj je
        best_pi = pi;       // zapamietaj dla jakiej permutacji
    }

    abc=shrage_abc(n,pi); // wyznacz a b c
    if(abc.q==-1)           // nie ma c?
        return UB;          // koniec, to jest optymalne wdl alg Caliera
    elem.r=9999999;         // inicjalizacja r'
    elem.q=9999999;         // inicjalizacja q'
    elem.p=0;               // inicjalizacja p'
    for(int i= abc.q +1; i<= abc.p; ++i)
    {
        if(elem.r > pi[i].r)
            elem.r=pi[i].r;
        if(elem.q > pi[i].q)
            elem.q=pi[i].q;
        elem.p = elem.p + pi[i].p;
    }

    identyfikator = pi[abc.q].id; // zapamietuje ktoremu elem zmieniam r
    stara_wartosc = pi[abc.q].r;
    pi[abc.q].r = max(pi[abc.q].r, elem.r+elem.p);
    LB = pre_shrage(n,pi);
    if(LB<UB)
        Calier(n,pi);
    for(int i=0;i<n;++i)
        if(pi[i].id == identyfikator)
        {
            abc.q=i;  // zadanie zostalo przesuniete
            pi[i].r = stara_wartosc;
            break;
        }
    identyfikator = pi[abc.q].id; // zapamietuje ktoremu elem zmieniam r
    stara_wartosc = pi[abc.q].q;
    pi[abc.q].q = max(pi[abc.q].q, elem.q + elem.p);
    LB = pre_shrage(n,pi);
    if(LB<UB)
        Calier(n,pi);
    for(int i=0;i<n;++i)
        if(pi[i].id == identyfikator)
        {
            abc.q=i;
            pi[i].q = stara_wartosc;
            break;
        }
    return UB;
}

int main()
{
	ifstream plik;
	string zmienna;

	cout << "Podaj nazwe pliku wejsciowego:  ";
	cin >> zmienna;
	cout << endl;

	/* otwieramy plik */
	plik.open(zmienna);

	/* sprawdzamy czy plik sie otworzyl */
	if (!plik.good() == true)
	{
		cout << "Nie mozna otworzyc pliku\n";
		return -1;
	}

	/* pobieramy liczbe zadan */
	plik >> n;

	/* unikamy bledu dla n<=0 */
	if (n <= 0)
	{
		cout << "Bledny plik z danymi\n";
		return -1;
	}

	vector<Dane> N;
	vector<Dane> G;
    vector<Dane> pi;


	/* wczytuje dane  */
	for (int i = 0; i < n; i++)
	{
	    N.push_back(e);
		/* koniec pliku gdy spodziewamy sie danych => blad */
		if (!plik.eof())
		{
			plik >> N[i].r;
		}
		else
		{
			cout << "bledny wymiar pliku\n";
			return -1;
		}

		/* koniec pliku gdy spodziewamy sie danych => blad */
		if (!plik.eof())
			plik >> N[i].p;
		else
		{
			cout << "bledny wymiar pliku\n";
			return -1;
		}
		/* koniec pliku gdy spodziewamy sie danych => blad */
		if (!plik.eof())
        {
			plik >> N[i].q;
			N[i].id = i;
        }
		else
		{
			cout << "bledny wymiar pliku\n";
			return -1;
		}
	}plik.close();


    cout<<"Schrage:  "<<shrage(n,N)<<endl;
    cout<<"Calier:   "<<Calier(n,N)<<endl;


	_getch();
	return 0;
}